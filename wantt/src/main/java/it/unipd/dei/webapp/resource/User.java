package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.sql.Date;

public class User extends Resource{
    private final String username;
    private final String first_name;
    private final String last_name;
    private final String password;
    private final String email;
    private final String gender;
    private final Date birthday;
    private final String country;
    private final String profile_pic;
    private final String about_me;
    private final String website;
    private final String education;
    private final int account_type;
    private final Date expire_date;
    /*private final ArrayList<String> company;*/

    public User(String username, String first_name, String last_name, String password, String email, String gender, Date birthday, String country, String profile_pic, String about_me, String website, String education, int account_type, Date expire_date/*, ArrayList<String> company*/) {
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
        this.email = email;
        this.gender = gender;
        this.birthday = birthday;
        this.country = country;
        this.profile_pic = profile_pic;
        this.about_me = about_me;
        this.website = website;
        this.education = education;
        this.account_type = account_type;
        this.expire_date = expire_date;
        /*this.company = company;*/

    }

    public String getUsername() {
        return username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getCountry() {
        return country;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public String getAbout_me() {
        return about_me;
    }

    public String getWebsite() {
        return website;
    }

    public String getEducation() {
        return education;
    }

    public int getAccount_type() {
        return account_type;
    }

    public Date getExpire_date() {
        return expire_date;
    }

    /*public ArrayList<String> getCompany() {
        return company;
    }*/


    @Override
    public final void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("user");

        jg.writeStartObject();

        jg.writeStringField("username", username);

        jg.writeStringField("password", password);

        jg.writeStringField("first_name", first_name);

        jg.writeStringField("last_name", last_name);

        jg.writeStringField("email", email);

        jg.writeStringField("gender", gender);

        jg.writeObjectField("birthday", birthday);

        jg.writeStringField("country", country);

        jg.writeStringField("profile_pic", profile_pic);

        jg.writeStringField("about_me", about_me);

        jg.writeStringField("website", website);

        jg.writeStringField("education", education);

        jg.writeNumberField("acoount_type", account_type);

        jg.writeObjectField("expire_date", expire_date);

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates a {@code User} from its JSON representation.
     *
     * @param in the input stream containing the JSON document.
     *
     * @return the {@code User} created from the JSON representation.
     *
     * @throws IOException if something goes wrong while parsing.
     */
    public static User fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        String jUsername = null;
        String jPassword = null;
        String jFirstName = null;
        String jLastName = null;
        String jEmail = null;
        String jGender = null;
        Date jBirthday = null;
        String jCountry = null;
        String jProfilePic = null;
        String jAboutMe = null;
        String jWebsite = null;
        String jEducation = null;
        int jAccountType = -1;
        Date jExpireDate = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "user".equals(jp.getCurrentName()) == false) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no employee object found.");
            }
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "username":
                        jp.nextToken();
                        jUsername = jp.getText();
                        break;
                    case "password":
                        jp.nextToken();
                        jPassword = jp.getText();
                        break;
                    case "first_name":
                        jp.nextToken();
                        jFirstName = jp.getText();
                        break;
                    case "last_name":
                        jp.nextToken();
                        jLastName = jp.getText();
                        break;
                    case "email":
                        jp.nextToken();
                        jEmail = jp.getText();
                        break;
                    case "gender":
                        jp.nextToken();
                        jGender = jp.getText();
                        break;
                    case "birthday":
                        jp.nextToken();
                        jBirthday = (Date) jp.getObjectId();
                        break;
                    case "country":
                        jp.nextToken();
                        jCountry = jp.getText();
                        break;
                    case "profile_pic":
                        jp.nextToken();
                        jProfilePic = jp.getText();
                        break;
                    case "about_me":
                        jp.nextToken();
                        jAboutMe = jp.getText();
                        break;
                    case "website":
                        jp.nextToken();
                        jWebsite = jp.getText();
                        break;
                    case "education":
                        jp.nextToken();
                        jEducation = jp.getText();
                        break;
                    case "expire_date":
                        jp.nextToken();
                        jExpireDate = (Date) jp.getObjectId();
                        break;
                }
            }
        }

        return new User(jUsername, jFirstName, jLastName, jPassword, jEmail, jGender, jBirthday, jCountry, jProfilePic, jAboutMe, jWebsite, jEducation, jAccountType, jExpireDate);
    }


}

//commento prova
