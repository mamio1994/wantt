package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateCompanyDatabase {

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "INSERT INTO wantt.company (name) VALUES (?)";

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * The employee to be stored into the database
     */
    private final Company company;

    /**
     * Creates a new object for storing an company into the database.
     *
     * @param con
     *            the connection to the database.
     * @param company
     *            the company to be stored into the database.
     */

    public CreateCompanyDatabase(Connection con, Company company) {
        this.con = con;
        this.company = company;
    }

    /**
     * Stores a new company into the database
     *
     * @throws SQLException
     *             if any error occurs while storing the company.
     */
    public void createCompany() throws SQLException {

        PreparedStatement pstmt = null;

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setString(1, company.getName());
            pstmt.execute();

        } finally {
            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

    }
}
