package it.unipd.dei.webapp.database;
import it.unipd.dei.webapp.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateUserDatabase {
    private static final String STMT= "INSERT INTO db.user(Username, Password, Email, FirstName, LastName, Gender, Birthday, Country, ProfilePicture, AboutMe, Website, Education, Account, ExpireDate) " +
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private final Connection CONN;
    private final User USER;


    public CreateUserDatabase(Connection CONN, User USER) {
        this.CONN = CONN;
        this.USER = USER;
    }

    public void createUser() throws SQLException{
        PreparedStatement pstmt = null;

        try{
            pstmt = CONN.prepareStatement(STMT);
            pstmt.setString(1, USER.getUsername());
            pstmt.setString(2, USER.getPassword());
            pstmt.setString(3, USER.getEmail());
            pstmt.setString(4, USER.getFirst_name());
            pstmt.setString(5, USER.getLast_name());
            pstmt.setString(6, USER.getGender());
            pstmt.setDate(7, USER.getBirthday());
            pstmt.setString(8, USER.getCountry());
            pstmt.setString(9, USER.getProfile_pic());
            pstmt.setString(10, USER.getAbout_me());
            pstmt.setString( 11, USER.getWebsite());
            pstmt.setString( 12, USER.getEducation());
            pstmt.setInt( 13, USER.getAccount_type());
            pstmt.setDate( 14, USER.getExpire_date());

            pstmt.execute();
        } finally {
            if (pstmt != null){
                pstmt.close();
            }
            CONN.close();
        }
    }
}
