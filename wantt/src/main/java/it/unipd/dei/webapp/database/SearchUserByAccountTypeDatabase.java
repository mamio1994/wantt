package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class SearchUserByAccountTypeDatabase {

    private static final String STATEMENT = "SELECT * FROM Ferro.Employee WHERE accounttype > ?";

    private final Connection con;
    private final int account_type;

    public  SearchUserByAccountTypeDatabase(final Connection con, final int account_type) {
        this.con = con;
        this.account_type = account_type;
    }

    public List<User> searchUserByAccountType() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        // the results of the search
        final List<User> users = new ArrayList<User>();

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, account_type);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                users.add(new User());
            }

        } finally {

            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }
            con.close();
        }

        return users;
    }