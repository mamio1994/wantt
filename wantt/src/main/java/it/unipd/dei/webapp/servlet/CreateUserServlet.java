package it.unipd.dei.webapp.servlet;



import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;
import it.unipd.dei.webapp.database.CreateUserDatabase;
import it.unipd.dei.webapp.resource.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class CreateUserServlet extends AbstractDatabaseServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        // request parameters
        String username = null;
        String first_name = null;
        String last_name = null;
        String password = null;
        String email = null;
        String gender = null;
        Date birthday = null;
        String country = null;
        String profile_pic = null;
        String about_me = null;
        String website = null;
        String education = null;
        int account_type = -1;
        Date expire_date = null;
// model
        User u = null;
        Message m = null;
        try {
            // retrieves the request parameters
            username = req.getParameter("username");
            first_name = req.getParameter("first_name");
            last_name = req.getParameter("last_name");
            password = req.getParameter("password");
            email = req.getParameter("email");
            gender = req.getParameter("gender");
            /*manca la data*/
            java.util.Date bday = new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter("birthday"));
            birthday = (Date) bday;
            country = req.getParameter("country");
            profile_pic = req.getParameter("profile_pict");
            about_me = req.getParameter("about_me");
            website = req.getParameter("web_site");
            ;
            education = req.getParameter("education");
            ;
            account_type = Integer.parseInt(req.getParameter("account_type"));
            java.util.Date ex_date = new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter("expire_date"));
            expire_date = (Date) ex_date;


            u = new User(username, first_name, last_name, password, email, gender, birthday, country, profile_pic, about_me, website, education, account_type, expire_date);

            // creates a new object for accessing the database and stores the employee
            new CreateUserDatabase(getDataSource().getConnection(), u).createUser();
            m = new Message(String.format("Employee %s successfully created.", username));
        } catch (NumberFormatException ex) {
            m = new Message("Cannot create the employee. Invalid input parameters: badge, age, and salary must be integer.",
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("23505")) {
                m = new Message(String.format("Cannot create the employee: employee %s already exists.", username),
                        "E300", ex.getMessage());
                m = new Message("Cannot create the employee: unexpected error while accessing the database.",
                        "E200", ex.getMessage());
            }
        } catch (ParseException ex) {

        }
        // stores the employee and the message as a request attribute
        req.setAttribute("employee", u);
        req.setAttribute("message", m);
        // forwards the control to the create-employee-result JSP
        req.getRequestDispatcher("/jsp/create-employee-result.jsp").forward(req, res);
    }
}

