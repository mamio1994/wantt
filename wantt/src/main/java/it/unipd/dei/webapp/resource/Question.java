package it.unipd.dei.webapp.resource;

public class Question {
    private final int id;
    private final String title;
    private final String description;
    private final String difficulty;
    private final String hint;
    private final boolean free;
    private final int view;
    private final String user;
    private final String category;


    public Question(int id, String title, String description, String difficulty, String hint, boolean free, int view, String user, String category) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.difficulty = difficulty;
        this.hint = hint;
        this.free = free;
        this.view = view;
        this.user = user;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public String getHint() {
        return hint;
    }

    public boolean isFree() {
        return free;
    }

    public int getView() {
        return view;
    }

    public String getUser() {
        return user;
    }

    public String getCategory() {
        return category;
    }
}

