package it.unipd.dei.webapp.resource;

public class Company {

    /**
     * The name of the company
     */
    private final String name;

    /**
     * Creates a new company
     *
     * @param name the name of the company
     */
    public Company(final String name) {
        this.name = name;
    }

    /**
     * Returns the name of the company.
     *
     * @return the name of the company.
     */
    public final String getName() {
        return name;
    }
}
