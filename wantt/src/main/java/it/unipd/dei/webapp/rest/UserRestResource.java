/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.*;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Manages the REST API for the {@link User} resource.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class UserRestResource extends RestResource {

    /**
     * Creates a new REST resource for managing {@code User} resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public UserRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
        super(req, res, con);
    }


    /**
     * Creates a new User into the database. 
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void createUser() throws IOException {

        User e  = null;
        Message m = null;

        try{

            final User user =  User.fromJSON(req.getInputStream());

            // creates a new object for accessing the database and stores the User
            e = new CreateUserDatabase(con, user).createUser();

            if(e != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                e.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot create the User: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the User: it already exists.", "E5A2", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the User: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Reads an User from the database.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void readUser() throws IOException {

        User e  = null;
        Message m = null;

        try{
            // parse the URI path to extract the badge
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("User") + 8);

            final int badge = Integer.parseInt(path.substring(1));


            // creates a new object for accessing the database and reads the User
            e = new ReadUserDatabase(con, badge).readUser();

            if(e != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                e.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("User %d not found.", badge), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot read User: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Updates an User in the database.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void updateUser() throws IOException {

        User e  = null;
        Message m = null;

        try{
            // parse the URI path to extract the badge
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("User") + 8);

            final int badge = Integer.parseInt(path.substring(1));
            final User User =  User.fromJSON(req.getInputStream());

            if (badge != User.getBadge()) {
                m = new Message(
                        "Wrong request for URI /User/{badge}: URI request and User resource badges differ.",
                        "E4A7", String.format("Request URI badge %d; User resource badge %d.",
                        badge, User.getBadge()));
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                m.toJSON(res.getOutputStream());
                return;
            }

            // creates a new object for accessing the database and updates the User
            e = new UpdateUserDatabase(con, User).updateUser();

            if(e != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                e.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("User %d not found.", User.getBadge()), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot update the User: other resources depend on it.", "E5A4", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update the User: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }


    /**
     * Deletes an User from the database.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void deleteUser() throws IOException {

        User e  = null;
        Message m = null;

        try{
            // parse the URI path to extract the badge
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("User") + 8);

            final int badge = Integer.parseInt(path.substring(1));


            // creates a new object for accessing the database and deletes the User
            e = new DeleteUserDatabase(con, badge).deleteUser();

            if(e != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                e.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("User %d not found.", badge), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot delete the User: other resources depend on it.", "E5A4", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot delete the User: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Searches Users by their salary.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void searchUserBySalary()  throws IOException {

        List<User> el  = null;
        Message m = null;

        try{
            // parse the URI path to extract the salary
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("salary") + 6);

            final int salary = Integer.parseInt(path.substring(1));


            // creates a new object for accessing the database and search the Users
            el = new SearchUserBySalaryDatabase(con, salary).searchUserBySalary();

            if(el != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(el).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot search User: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search User: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Lists all the Users.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void listUser() throws IOException {

        List<User> el  = null;
        Message m = null;

        try{
            // creates a new object for accessing the database and lists all the Users
            el = new ListUserDatabase(con).listUser();

            if(el != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(el).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list Users: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search User: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
