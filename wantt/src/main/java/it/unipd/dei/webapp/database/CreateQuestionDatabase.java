package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Question;
import it.unipd.dei.webapp.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateQuestionDatabase {
    private static final String STMT= "INSERT INTO wantt.quesiton(id, title, description, difficulty, hint, free, view, user, category) " +
            "VALUES (?,?,?,?,?,?,?,?,?)";

    private final Connection CONN;
    private final Question QUEST;


    public CreateQuestionDatabase(Connection CONN, Question QUEST) {
        this.CONN = CONN;
        this.QUEST = QUEST;
    }


    public void createUser() throws SQLException {
        PreparedStatement pstmt = null;

        try{
            pstmt = CONN.prepareStatement(STMT);
            pstmt.setInt(1, QUEST.getId());
            pstmt.setString(2, QUEST.getTitle());
            pstmt.setString(3, QUEST.getDescription());
            pstmt.setString(4, QUEST.getDifficulty());
            pstmt.setString(5, QUEST.getHint());
            pstmt.setBoolean(6, QUEST.isFree());
            pstmt.setInt(7, QUEST.getView());
            pstmt.setString(8, QUEST.getUser()); /*DUBBIO*/
            pstmt.setString(9, QUEST.getCategory());

            pstmt.execute();
        } finally {
            if (pstmt != null){
                pstmt.close();
            }
            CONN.close();
        }
    }
}
